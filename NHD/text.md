# The Light bulb

The light bulb was patented by Thomas Edison in 1879. The light bulb was significant because it allowed people to have a long-lasting (compared to what they had before) & safe light source in their homes. It works by sending an electric current through a filament.

The light bulb is much safer than previous lighting methods such as oil lamps and candles. Both oil lamps and candles use fire to produce light. This means that if you keep an oil lamp or candle on without monitoring it, it could set your house on fire. However, the light bulb uses electricity to produce light, which is much safer.

Light bulbs are also much easier to maintain and more efficient than oil lamps and candles. Oil lamps and candles use a small fire to produce light which goes out easily. However, light bulbs send an electric current through a filament to produce light so they only stop working once the filament breaks. This makes them much more appealing for homeowners.

This proves that light bulbs were a very important invention and were safer and more reliable than their predecessors.