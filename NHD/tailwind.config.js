/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: "class",
  content: ["./**.{html,js}"],
  theme: {
    extend: {
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
        noto: ["Noto Sans", "sans-serif"],
      },
      typography: {
        DEFAULT: {
          css: {
            textAlign: "center",
            h1: {
              fontFamily: "Poppins",
              fontWeight: 500,
              marginBottom: "8px",
            },
            p: {
              marginTop: "8px",
              fontFamily: "Noto Sans",
            },
          },
        },
      },
      outlineOffset: {
        4: "-4px",
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
