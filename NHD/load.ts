function showelements() {
  //Uncomment the line below if you want the loader to hide on page load
  document
    .getElementsByClassName("centeredcontainer")[0]
    .classList.add("fadeout");
  setTimeout(() => {
    document
      .getElementsByClassName("centeredcontainer")[0]
      .classList.add("hidden");
  }, 1000);
  var allElements = document.getElementsByTagName("*");
  setTimeout(() => {
    for (let element of allElements) {
      if (
        element.classList.contains("centeredcontainer") != true &&
        element.tagName != "HTML"
      ) {
        element.classList.add("fadein");
      }
    }
  }, 1000);
}

function hideload() {
  console.log("Loaded");
  setTimeout(showelements, 5000);
}

window.onload = hideload();
