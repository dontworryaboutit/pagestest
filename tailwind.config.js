/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**.{html,js}"],
  theme: {
    extend: {
      outlineOffset: {
        4: '-4px',
      }
    },
  },
  plugins: [],
}
